#include <WiFi.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#define EEPROM_SIZE 2

#define INTERVAL_PIR 10000
#define INTERVAL_LIGHT 100
#define INTERVAL_PIR_ALARM 2000
 
unsigned long time_pir = 0;
unsigned long time_light = 0;
unsigned long time_alarm = 0;


// piny na ESP32 dev kite
//motorcek
const int in1 =  13;
const int in2 =  12;
const int in3 = 14;
const int in4 = 27;
//pir
const int pir_sensor = 2;  
const int lockLED = 4;
const int pirLED = 18;
//ventilator
const int vent_pin = 5;
//photoresistor
const int photo_res = 34;

const int permAdress = 0;

//vacsie cislo mensia rychlost
int turn_speed = 2;
//uhol otocenia
int angle = 90;

const char* ssid = "rpi-gateway";
const char* password = "solution";
const char* mqtt_server = "10.3.141.1";
const char* clientID = "GARAGE_ESP32";
const char* outTopic = "error";
const char* doorTopic = "action/door";
const char* ventTopic = "action/vent";
const char* lockTopic = "action/lock";
const char* garageDoorID = "garage1";
const char* ventID = "vent1";

WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];
char completeMsg[50];

//bool pre posledny stav dveri
int opened = 0;
bool vent_run = false;
//bool pre photoresistor
bool night = false;

void setup() {
  if (!EEPROM.begin(EEPROM_SIZE)) //flash permanent memory inicializacia
  {
    Serial.println("failed to initialise EEPROM"); delay(1000000);
  }

  if(EEPROM.read(permAdress)!=0 || EEPROM.read(permAdress)!=2){
    EEPROM.write(permAdress, 0);
    EEPROM.commit();
  }  
  
  Serial.begin(115200); 
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT); 
  pinMode(vent_pin, OUTPUT);
  pinMode(photo_res, ANALOG);

  pinMode(pir_sensor, INPUT);   // declare sensor as input  
  pinMode(lockLED, OUTPUT);   // declare led as output
  pinMode(pirLED, OUTPUT);    // declare led as output
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  Serial.print("Last state of motor is: "); Serial.println(byte(EEPROM.read(permAdress)));  
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop(); 

  int pir_status = digitalRead(pir_sensor);   // read sensor value
  //zachyteny pohyb
  if (pir_status == HIGH) {    
    if(millis() > time_pir + INTERVAL_PIR){
        time_pir = millis();
        //print_time(time_1);
        digitalWrite(pirLED, HIGH);
        send_pir_status();
    }   
  }
  //necinne PIR
  else{
    if(millis() > time_alarm + INTERVAL_PIR_ALARM){
      digitalWrite(pirLED, LOW);      
    }
  }
  
  //interval pre rozsvietenie PIR led
  if(millis() > time_light + INTERVAL_LIGHT){
        time_light = millis();
        check_light_intensity();
  }
}

void rotationClockwise() {
  for(int i=0;i<(angle*64/45);i++){
    step1();
    step2();
    step3();
    step4();
    step5();
    step6();
    step7();
    step8();
  }
}
void rotationCounterClockwise() {
  for(int i=0;i<(angle*64/45);i++){
    step8();
    step7();
    step6();
    step5();
    step4();
    step3();
    step2();
    step1();
  }
}
// krokovanie motorceka
void step1(){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(turn_speed);
}
void step2(){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(turn_speed);
}
void step3(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  delay(turn_speed);
}
void step4(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  delay(turn_speed);
}
void step5(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  delay(turn_speed);
}
void step6(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, HIGH);
  delay(turn_speed);
}
void step7(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  delay(turn_speed);
}
void step8(){
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  delay(turn_speed);
}

void setup_wifi() {

  delay(10);
  // pripajanie na wifi
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop kym sa znova pripojime
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // pokus na pripojenie
    if (client.connect(clientID)) {
      Serial.println("connected");
      // test publish po pripojenoi
      client.publish(outTopic, clientID);
      // odebrania
      client.subscribe(doorTopic);
      client.subscribe(ventTopic);
      client.subscribe(lockTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // bezpecnostny delay
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {

  payload[length] = '\0';
  String message = (char*)payload;
   
   char separator[] = ",";
   char *token;
    Serial.println(String(topic));
   
   if(String(topic) == String(doorTopic)){
      if(message.startsWith(garageDoorID)){  
  
       Serial.println(message);     
    
        token = strtok((char*)payload, separator);
        token = strtok(NULL, separator);   
        Serial.println("Garaz sa ma: "+(String)token);
        opened = byte(EEPROM.read(permAdress));
        Serial.print("Opened is: ");
        Serial.println(byte(EEPROM.read(permAdress)));
        
        if((strcmp((char *)token, "open") == 0) && (opened==0)){
          rotationClockwise();
          EEPROM.write(permAdress, 2);  //1 je default, ked je flash memory prazdna
          EEPROM.commit();
        }
        else if((strcmp((char *)token, "close") == 0) && (opened==2)){
          rotationCounterClockwise();
          EEPROM.write(permAdress, 0);
          EEPROM.commit();
        }
     
      
      }
   }
   else if(String(topic) == String(ventTopic)){    
    if(message.startsWith(ventID)){  
  
       Serial.println(message);     
    
        token = strtok((char*)payload, separator);
        token = strtok(NULL, separator);   
        Serial.println("Ventilator sa ma: "+(String)token);
        
        if((strcmp((char *)token, "on") == 0) && (vent_run==false)){
          digitalWrite(vent_pin, HIGH);
          vent_run = true;
        }
        else if((strcmp((char *)token, "off") == 0) && (vent_run==true)){
          digitalWrite(vent_pin, LOW);
          vent_run = false;
        }      
      }
   }
   else if(String(topic) == String(lockTopic)){
      if(strcmp((char *)payload, "1"))
        digitalWrite(lockLED, HIGH);
      else
        digitalWrite(lockLED, LOW);
    }     
}

void check_light_intensity(){
  int sensorValue = analogRead(photo_res);   
  // konverzia analog hodnoty na hodnotu napatia
  float voltage = sensorValue * (3.3 / 1023.0);
  
  dtostrf(voltage, 2, 2, msg);

  if(voltage<4 && night!=true){   //detekovanie zmeny stavu medzi tmou a svetlom, zabranenie preblikavaniu
    night=true;
    client.publish("sensor/ldr", msg);    
  }
  else if(voltage>5 && night==true){
    night=false;
    client.publish("sensor/ldr", msg);
  } 
}

void send_pir_status(){
  Serial.println("Presence detected from PIR");
  client.publish("sensor/pir", "1"); 
}
