
/*
  MQTT DHT22, when "temperature, c" is sent it returns the temperature in celcius
  when "humidity" is sent it returns the humidity as measured by the DHT22
  the signal pin is connected to a pull-ip resistor and GPIO 2
*/
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

#define DHTTYPE DHT11

const char* sensorId = "1"; 
const char* sensorId2 = "2"; 

const char* ssid = "rpi-gateway";
const char* password = "solution";
const char* mqtt_server = "10.3.141.1";
const char* clientID = "DHT11_ESP_1";
const char* outTopic = "error";
const char* inTopic = "NodeMCUin";
const int sleepTime = 600000;

DHT dht(0, DHTTYPE, 11); // 11 works fine for ESP8266
DHT dht2(2, DHTTYPE, 11); // 11 works fine for ESP8266

float humidity, temp_c;  // Values read from sensor
unsigned long previousMillis = 0;        // will store last temp was read
const long interval = 2000;              // interval at which to read sensor
 
WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];
char completeMsg[50];

void setup_wifi() {

  delay(10);
  // Connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(10);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void sendMeasurements(){
  //prvy dht senzor
    if(gettemperature(dht)){
      Serial.print("Sending temperature:");
      Serial.println(temp_c);    
      dtostrf(temp_c , 2, 2, msg);
      strcpy(completeMsg, sensorId);
      strcat(completeMsg, "_");
      strcat(completeMsg, msg);    
      client.publish("sensor/temp", completeMsg);
  
      memset(completeMsg, 0, sizeof(completeMsg));
      dtostrf(humidity , 2, 2, msg);
      strcpy(completeMsg, sensorId);
      strcat(completeMsg, "_");
      strcat(completeMsg, msg);
      client.publish("sensor/hum", completeMsg);
    }
    else{
      Serial.println("Measurement failed!");
      client.publish("error", "Error on dht1");
    }
    delay(5000);
//druhy dht senzor
    if(gettemperature(dht2)){
      Serial.print("Sending temperature:");
      Serial.println(temp_c);    
      dtostrf(temp_c , 2, 2, msg);
      strcpy(completeMsg, sensorId2);
      strcat(completeMsg, "_");
      strcat(completeMsg, msg);    
      client.publish("sensor/temp", completeMsg);
  
      memset(completeMsg, 0, sizeof(completeMsg));
      dtostrf(humidity , 2, 2, msg);
      strcpy(completeMsg, sensorId2);
      strcat(completeMsg, "_");
      strcat(completeMsg, msg);
      client.publish("sensor/hum", completeMsg);
    }
    else{
      Serial.println("Measurement failed!");
      client.publish("error", "Error on dht2");
    }
}


void callback(char* topic, byte* payload, unsigned int length) {
  // Conver the incoming byte array to a string
  payload[length] = '\0'; // Null terminator used to terminate the char array
  String message = (char*)payload;

  Serial.print("Message arrived on topic: [");
  Serial.print(topic);
  Serial.print("], ");
  Serial.println(message);

  if(message == "temperature, c"){
    if(gettemperature(dht)){
      Serial.print("Sending temperature:");
      Serial.println(temp_c);
      dtostrf(temp_c , 2, 2, msg);
      client.publish(outTopic, msg);
    }    
  } else if (message == "humidity"){
    if(gettemperature(dht)){
      Serial.print("Sending humidity:");
      Serial.println(humidity);
      dtostrf(humidity , 2, 2, msg);
      client.publish(outTopic, msg);
    }    
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientID)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(outTopic, clientID);
      // ... and resubscribe
      //client.subscribe(inTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  dht.begin();           // initialize temperature sensor
  dht2.begin();

}

void loop() {

  if (!client.connected()) {
    reconnect();
  }    
  sendMeasurements(); 
  client.loop();   
  delay(sleepTime);  
}

bool gettemperature(DHT dht) {
  
  float hum = dht.readHumidity();          // Read humidity (percent)
  float temp = dht.readTemperature();     // Read temperature as Celcius    
 
  if (isnan(hum) || isnan(temp)) {
    Serial.println("Failed to read from DHT sensor!");
    return false;
  }
  humidity = hum;
  temp_c = temp;    

return true;
}
