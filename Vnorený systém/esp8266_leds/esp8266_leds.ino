#include <ESP8266WiFi.h>
#include <PubSubClient.h>

int light1PIN= 0;
int light2PIN = 2;

const char* lampId = "light1"; 
const char* lampId2 = "light2"; 

// Update these with values suitable for your network.

//const char* ssid = "INFOTECH";
const char* ssid = "rpi-gateway";
//const char* password = "MU1nFotech28";
const char* password = "solution";
//const char* mqtt_server = "192.168.30.47";
const char* mqtt_server = "10.3.141.1";
const char* clientID = "LED_ESP_1";
const char* outTopic = "error";
const char* inTopic = "action/light";
const int sleepTimeS = 10;


unsigned long previousMillis = 0;        // will store last temp was read
const long interval = 2000;              // interval at which to read sensor
 
WiFiClient espClient;
PubSubClient client(espClient);
char msg[50];
char completeMsg[50];

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void callback(char* topic, byte* payload, unsigned int length) {
  // Conver the incoming byte array to a string
  payload[length] = '\0'; // Null terminator used to terminate the char array
  String message = (char*)payload;

   char input[] = "12,14,15";
   char separator[] = ",";
   char *token;

   Serial.println(message);
    
  if(message.startsWith(lampId)){
    Serial.print("lampa 1 zvolena\n");

    token = strtok((char*)payload, separator);   
    Serial.println("id je "+(String)token);
    token = strtok(NULL, separator);
    if(strcmp((char *)token, "1") == 0){
      digitalWrite(light1PIN, HIGH);
    }
    else{
      digitalWrite(light1PIN, LOW);
    }   
    Serial.println("Nastavenie led na "+(String)token);
  }
  else if(message.startsWith(lampId2)){
    Serial.print("lampa 2 zvolena\n");

    token = strtok((char*)payload, separator);   
    Serial.println("id je "+(String)token);
    token = strtok(NULL, separator);
    if(strcmp((char *)token, "1") == 0){
      digitalWrite(light2PIN, HIGH);
    }
    else{
      digitalWrite(light2PIN, LOW);
    }   
    Serial.println("Nastavenie led na "+(String)token);
  }
  else{
    Serial.print("Neplatna lampa\n");
  } 
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientID)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(outTopic, clientID);
      // ... and resubscribe
      client.subscribe(inTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(light1PIN, OUTPUT);
  pinMode(light2PIN, OUTPUT); 
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);  
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();  
}
