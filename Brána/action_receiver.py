import requests
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import time
from time import sleep

MQTT_SERVER = "localhost"
client = mqtt.Client()


print("action_receiver run!")


def init():
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_disconnect = on_disconnect

    client.connect(MQTT_SERVER, 1883, 60)
    client.loop_start()

    try:
        while True:            

            try:
                response = requests.get(
                'http://smarthome.infotech.sk:8080/Services/MongoInterface.svc/Actions', headers={'AuthorizationDevice':'XXX'})

                print(response.text)
                response_obj = json.loads(response.text)
                print(response.status_code, response.reason)  # HTTP
                for obj in response_obj:

                    if (obj['t']==1):
                        if (obj['s']):
                            send_message("action/light", obj['uuid'] + ",1")

                        else:
                            send_message("action/light", obj['uuid'] + ",0")

                    elif (obj['t']==2):
                        if (obj['s']):
                            send_message("action/door", obj['uuid'] + ",open")

                        else:
                            send_message("action/door", obj['uuid'] + ",close")
                            
                    elif (obj['t']==3):
                        if (obj['s']):
                            send_message("action/vent", obj['uuid'] + ",on")

                        else:
                            send_message("action/vent", obj['uuid'] + ",off")
                            
                    elif (obj['t']==4):                        
                        send_message("sensor/pir", "1")                           

            except Exception as e:
                sleep(60)
                print(e)

            sleep(10)

    except Exception as e:
        print(e)
        init()


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))


def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass

def on_disconnect(client, userdata, rc):
    print("Action_receiver MQTT client was disconnected")
    pass

def send_message(topic, msg):
    client.publish(topic, msg)


init()
