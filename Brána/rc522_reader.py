#!/usr/bin/python
#--------------------------------------
#    ___  ___  _ ____          
#   / _ \/ _ \(_) __/__  __ __ 
#  / , _/ ___/ /\ \/ _ \/ // / 
# /_/|_/_/  /_/___/ .__/\_, /  
#                /_/   /___/   
#
#  RC522 Simple Read Example
#
# This script requires a RC522 RFID reader
# with SPI enabled.
# 
# It uses the following libraries :
# https://github.com/lthiery/SPI-Py.git
# https://github.com/mxgxw/MFRC522-python
#
# Please see https://www.raspberrypi-spy.co.uk/
# for more information.
#
# Author : Matt Hawkins
# Date   : 23/02/2018
#
#--------------------------------------

import time
import RPi.GPIO as GPIO
from lib import MFRC522
import paho.mqtt.client as mqtt

MQTT_SERVER = "10.3.141.1"
client = mqtt.Client()
unlocked = False
# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# Welcome message
print("Looking for cards")

# This loop checks for chips. If one is near it will get the UID
def init():
    try:
        client.on_connect = on_connect
        client.on_publish = on_publish
        client.on_disconnect = on_disconnect

        client.connect(MQTT_SERVER, 1883, 60)
        client.loop_start()

        while True:

            # Scan for cards
            (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

            # Get the UID of the card
            (status,uid) = MIFAREReader.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == MIFAREReader.MI_OK:

              # Print UID
              full_id= str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
              print("Card UID: "+full_id)
              send_message("action/rfid", full_id)

              time.sleep(1)

    except Exception as e:
        print(e)
        GPIO.cleanup()
        init()

def send_message(topic, msg):
    client.publish(topic, msg)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))


def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass

def on_disconnect(client, userdata, rc):
    print("Action_receiver MQTT client was disconnected")
    pass


init()
