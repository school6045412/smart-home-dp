import os
from multiprocessing import Pool

processes = ('rc522_reader.py', 'data_sender.py', 'action_receiver.py')
             
def run_process(process):
    os.system('python {}'.format(process))

if __name__ == '__main__':
    pool = Pool(processes=3)    
    pool.map(run_process, processes)




