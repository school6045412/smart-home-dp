import base64

import requests
import paho.mqtt.client as mqtt
import json
import time
import threading
import picamera
from time import sleep
#from rc522_reader import unlocked

locked = False

MQTT_SERVER = "localhost"
MQTT_PATH = "test_channel"


data = {}
data2 = {}
data['key'] = 'value'
data2['key'] = 'value'

print("data_sender run!")

def convertImageToBase64():
    with open("image_test.jpg", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
    return encoded


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe([("sensor/pir", 0), ("sensor/temp", 0), ("sensor/hum", 0), ("action/light", 0), ("sensor/ldr", 0), ("action/rfid",0)])



def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass



# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):

    print(msg.topic + " " + str(msg.payload))
    
    #if unlocked:
        #print("System is unlocked!")
              
    #else:
         #print("System is locked!") 
    try:
        
        if (msg.topic == "sensor/temp"):
            print("Posielanie teploty")
            payload = str(msg.payload.decode()).split('_', 1)
            id = payload[0]
            timestamp = int(time.time())
            value = payload[1]

            #headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
            data = [{'uuid': 'sen_temp_' + id, 't': timestamp, 'v': value}]

            response = requests.post('http://smarthome.infotech.sk:8080/Services/MongoInterface.svc/Data', json=data, headers={'AuthorizationDevice': 'XXX'})
            print(response.text)
            print(response.status_code, response.reason)  # HTTP

        elif(msg.topic == "sensor/hum"):
            payload = str(msg.payload.decode()).split('_', 1)
            id = payload[0]
            timestamp = int(time.time())
            value = payload[1]

            # headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
            data = [{'uuid': 'sen_hum_' + id, 't': timestamp, 'v': value}]

            response = requests.post('http://smarthome.infotech.sk:8080/Services/MongoInterface.svc/Data', json=data, headers={'AuthorizationDevice': 'XXX'})
            print(response.text)
            print(response.status_code, response.reason)  # HTTP
            
        elif (msg.topic == "action/rfid"):
            global locked
            payload = str(msg.payload.decode())
            print("System registered tag with id: "+payload)
            
            if locked:
                client.publish("action/lock", "0")
                locked=False
            else:
                client.publish("action/lock", "1")
                locked=True
                  

        elif(msg.topic == "sensor/pir"):        
            payload = str(msg.payload.decode())
            id = payload
            timestamp = int(time.time())
            
            data = [{'uuid': 'sen_pir_' + id, 't': timestamp, 'v': 1}]
            
            response = requests.post('http://smarthome.infotech.sk:8080/Services/MongoInterface.svc/Data', json=data, headers={'AuthorizationDevice': 'XXX'})
            print(response.text)
            print(response.status_code, response.reason)
            print("prijatie poziadavky na fotku od pir cislo: "+id)

            camera = picamera.PiCamera()

            try:
                camera.start_preview()
                sleep(1)            
                camera.capture('image_test.jpg', resize=(1296, 730))
                camera.stop_preview()
                pass
            finally:
                camera.close()

            # headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
            #data = [{'uuid': 'sen_pir_' + id, 't': timestamp, 'd': data}]
                data2 = {'t': timestamp, 'd': convertImageToBase64()}
            
            #print(data)

                response2 = requests.post('http://smarthome.infotech.sk:8080/Services/MongoInterface.svc/Image',
                                     json=data2, headers={'AuthorizationDevice': 'XXX'})
                print(response2.text)
                print(response2.status_code, response2.reason)  # HTTP

        elif(msg.topic == "sensor/ldr"):
            payload = float(msg.payload.decode())
            value = payload
            print("Svetelne podmienky: "+str(value) + " V")
            
            if(value<5):
                client.publish("action/light", "light1,1")
                client.publish("action/light", "light2,1")
            elif(value>6):
                client.publish("action/light", "light1,0")
                client.publish("action/light", "light2,0")
    except Exception as e:
        print(e)


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_publish = on_publish


client.connect(MQTT_SERVER, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()




