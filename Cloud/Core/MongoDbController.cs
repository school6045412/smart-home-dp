﻿using Core.ApiModel;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Hosting;


namespace Core
{
    public class MongoDbController
    {
        private const string groupString = @"{$group: {                
                    '_id': '$uuid',                    
                     data: {$last: '$v'},
                     date: {$last: '$t'},
                }},
                {$sort: {
                    't': -1
                }}";

        private const string projectString = @"
                {$project: {
                    '_id': 0,
                    'uuid': '$_id',
                    'v': '$data',
                    't': '$date'
                }}";

        private const string intervalDataString = @"
                {$project: {
                    '_id': 0,
                    'uuid': '$_id',
                    'v': '$data',
                    't': '$date'
                }}";

        private const string deleteFilter = @"{
                't' : { $lte: 1544101985}
                }";



        private string CONNECTION_STRING = ConfigurationManager.ConnectionStrings["MONGO_DB"].ConnectionString;
        private string DBNAME = "martin_sensors";
        private MongoClient client;
        private IMongoDatabase db;

        public MongoDbController()
        {
            MongoDefaults.MaxConnectionIdleTime = TimeSpan.FromMinutes(5);
            client = new MongoClient(CONNECTION_STRING);
            db = client.GetDatabase(DBNAME);
        }

        public IEnumerable<Device> GetDevices()
        {
            return db.GetCollection<Device>(Device.PREFIX).Find(d => true).ToList();
        }


        public IEnumerable<Device> GetActions()
        {
            var filter = Builders<Device>.Filter.Eq(d => d.Processed, false);
            var update = Builders<Device>.Update.Set(d => d.Processed, true);

            var newActions = db.GetCollection<Device>(Device.PREFIX).Find(d => d.Processed == false).ToList();
            db.GetCollection<Device>(Device.PREFIX).UpdateManyAsync(filter, update);

            return newActions;
        }

        public void InsertSensorsData(IEnumerable<SensorData> data)
        {

            db.GetCollection<SensorData>(SensorData.PREFIX).InsertMany(data);
        }

        public IEnumerable<SensorData> GetActualData()
        {
            BsonDocument group = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(groupString);
            BsonDocument project = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(projectString);
            var pipeline = new[] { group, project };
            var col = db.GetCollection<SensorData>(SensorData.PREFIX);
            return col.Aggregate<SensorData>(pipeline).ToList();
        }

        public async Task<IEnumerable<SensorData>> GetDateIntervalData(int from, int to, int type)
        {
            List<SensorData> data;

            switch (type)
            {
                case 1:      //temperature
                    {
                        data = await db.GetCollection<SensorData>(SensorData.PREFIX).Find(x => x.Timestamp >= from && x.Timestamp < to && x.UUID.StartsWith("sen_temp"))
                            .SortByDescending(x => x.Timestamp).Limit(1000).ToListAsync();
                        break;
                    }

                case 2:     //humidity
                    {
                        data = await db.GetCollection<SensorData>(SensorData.PREFIX).Find(x => x.Timestamp >= from && x.Timestamp < to && x.UUID.StartsWith("sen_hum"))
                            .SortByDescending(x => x.Timestamp).Limit(1000).ToListAsync();
                        break;
                    }
                case 3:     //pir
                    {
                        data = await db.GetCollection<SensorData>(SensorData.PREFIX).Find(x => x.Timestamp >= from && x.Timestamp < to && x.UUID.StartsWith("sen_pir"))
                            .SortByDescending(x => x.Timestamp).Limit(100).ToListAsync();
                        break;
                    }
                default:
                    {
                        data = null;
                        break;
                    }
            }

            return data;
        }

        public async void UpdateDevice(Device device)
        {
            var filter = Builders<Device>.Filter.Eq(d => d.UUID, device.UUID);
            var update = Builders<Device>.Update.Set(d => d.Status, device.Status).
                                                 Set(d => d.Value, device.Value).
                                                 Set(d => d.Processed, false);

            var col = db.GetCollection<Device>(Device.PREFIX);
            var result = await col.UpdateOneAsync(filter, update);
        }

        public async Task<IEnumerable<Image>> GetLastImages()
        {
            return await db.GetCollection<Image>(Image.PREFIX).Find(x => true)
                            .SortByDescending(x => x.Timestamp).Limit(10).ToListAsync();
        }

        public async void SaveImage(Image img)
        {
            byte[] bytes = Convert.FromBase64String(img.BinaryData);

            string filePath = HostingEnvironment.MapPath("~/../Images/") + img.Timestamp + ".jpg";
            File.WriteAllBytes(filePath, bytes);
            
            await db.GetCollection<Image>(Image.PREFIX).InsertOneAsync(img);
        }

        public void DeleteOldImages()
        {
            int olderThan = ((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds) - 7 * 3600 * 24;
            var deletedCount = db.GetCollection<Image>(Image.PREFIX).DeleteMany(t => t.Timestamp < olderThan).DeletedCount;
        }




    }
}