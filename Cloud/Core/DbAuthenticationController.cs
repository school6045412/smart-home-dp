﻿using Core.ApiModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class DbAuthenticationController
    {
        private static string DEVICE_TOKEN = ConfigurationManager.AppSettings["DEVICE_TOKEN"] as string;
        private static MongoDbController mongoDbController = new MongoDbController();

        public static bool AuthorizeDeviceToken(string token)
        {
            return DEVICE_TOKEN.Equals(token);
        }

        public static User GetUser(string email, string password)
        {
            return mongoDbController.GetUser(u => u.Email == email);
        }
    }
}
