﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public enum DeviceType : byte
    {
        Lamp= 1,
        Motor= 2,
        Thermometer = 3
    }
}
