﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class AuthenticationController
    {
        private static string DEVICE_TOKEN = ConfigurationManager.AppSettings["DEVICE_TOKEN"] as string;

        public static bool AuthorizeDeviceToken(string token)
        {
            return DEVICE_TOKEN.Equals(token);
        }
    }
}
