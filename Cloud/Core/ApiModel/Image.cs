﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApiModel
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class Image
    {
        [IgnoreDataMember]
        public ObjectId Id { get; set; }
        [DataMember(Name = "t")]
        [BsonElement("t")]
        public int Timestamp { get; set; }   //code name
        [DataMember(Name = "d")]
        [BsonIgnore]
        public string BinaryData { get; set; }

        public const string PREFIX = "Images";
    }
}
