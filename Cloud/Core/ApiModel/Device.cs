﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApiModel
{
    [DataContract]
    public class Device
    {                       
        [DataMember]
        public ObjectId Id { get; set; }    //identificator
        [DataMember(Name = "uuid")]         
        [BsonElement("uuid")]
        public string UUID { get; set; }   //code name
        [DataMember(Name = "n")]
        [BsonElement("n")]
        public string Name { get; set; }    //human readable name
        [DataMember(Name = "t")]
        [BsonElement("t")]
        DeviceType Type { get; set; }    //device type
        [DataMember(Name = "s")]
        [BsonElement("s")]
        public bool Status { get; set; }       //turn on/off
        [DataMember(Name = "v")]
        [BsonElement("v")]
        public string Value { get; set; }      //more info, color etc.
        [DataMember(Name = "lto")]
        [BsonElement("lto")]
        public int LastTimeOnline { get; set; }     //identificator of household
        [DataMember(Name = "gt")]
        [BsonElement("gt")]
        public string GatewayToken { get; set; }      //smarthome installation identification
        [DataMember(Name = "p")]
        [BsonElement("p")]
        public bool Processed { get; set; }     //device processed after last change


        public const string PREFIX = "DevicesConfig";
    }
}
