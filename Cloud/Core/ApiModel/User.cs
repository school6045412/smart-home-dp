﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApiModel
{
    [DataContract]
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [DataMember]
        public string Id { get; set; }

        [Display(Name = "Name")]
        [BsonElement("name")]
        [DataMember]
        public string Name { get; set; }

        [Display(Name = "Surname")]
        [BsonElement("surname")]
        [DataMember]
        public string Surname { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Mail")]
        [BsonElement("mail")]
        [DataMember]
        public string Email { get; set; }

        [Required(ErrorMessage = "Login is required")]
        [Display(Name = "Login")]
        [BsonElement("login")]
        [DataMember]
        public string Login { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [BsonElement("psswd")]
        [DataMember]
        public string Password { get; set; }

        [BsonElement("hash")]
        public string Hash { get; set; }

        [BsonElement("role")]
        public string Role { get; set; }

        public bool RememberMe { get; set; }

        public const string PREFIX = "UsersConfig";
    }
}
