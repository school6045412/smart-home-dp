﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApiModel
{
    [DataContract]    
    public class SensorData
    {        
        [IgnoreDataMember]
        public ObjectId Id { get; set; }    //mongoDB id
        [DataMember(Name = "uuid")]     
        [BsonElement("uuid")]
        public string UUID { get; set; }    //identificator
        [DataMember(Name = "t")]
        [BsonElement("t")]
        public int Timestamp { get; set; }  //timestamp of measurement
        [DataMember(Name = "v")]
        [BsonElement("v")]
        public string Value { get; set; }

        public const string PREFIX = "DataHistory";
    }

    
}
