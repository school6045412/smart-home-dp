﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.ApiModel
{
    [DataContract]
    class Gateway
    {
        [DataMember]
        [BsonElement("token")]
        string UUID { get; set; }
        [DataMember]
        [BsonElement("uuid")]
        string Name { get; set; }
    }
}
