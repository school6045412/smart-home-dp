﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class Crypto
    {
        private const int hashIterations = 1000;
        private const int hashLength = 32;

        /// <summary>
        /// Generates salted hash from text input
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static byte[] GenerateSaltedHash(string plainText)
        {
            return GenerateSaltedHash(Encoding.UTF8.GetBytes(plainText), GenerateSalt());
        }

        /// <summary>
        /// Generates salted hash from text input
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static byte[] GenerateSaltedHash(byte[] plainText)
        {
            return GenerateSaltedHash(plainText, GenerateSalt());
        }

        /// <summary>
        /// Generates salted hash from text input
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GenerateSaltedHash(string plainText, byte[] salt)
        {
            return GenerateSaltedHash(Encoding.UTF8.GetBytes(plainText), salt);
        }

        /// <summary>
        /// Generates salted hash from text input
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            return new Rfc2898DeriveBytes(plainText, salt, hashIterations).GetBytes(hashLength);
        }

        /// <summary>
        /// Generates random salt
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateSalt()
        {
            var random = new RNGCryptoServiceProvider();

            // Empty salt array
            byte[] salt = new byte[hashLength];

            // Build the random bytes
            random.GetNonZeroBytes(salt);
            return salt;
        }

        /// <summary>
        /// Generates random password
        /// </summary>
        /// <returns></returns>
        public static string GeneratePassword()
        {
            return Guid.NewGuid().ToString("N").ToLower()
                      .Replace("1", "").Replace("o", "").Replace("0", "")
                      .Substring(0, 6);
        }

        /// <summary>
        /// Validates equalation of input hashed with salt to hash etalon
        /// </summary>
        /// <param name="input">>input to compare</param>
        /// <param name="salt">salt</param>
        /// <param name="hash">etalon</param>
        /// <returns></returns>
        public static bool ValidateHash(string input, byte[] salt, byte[] hash)
        {
            return ValidateHash(Encoding.UTF8.GetBytes(input), salt, hash);
        }

        /// <summary>
        /// Validates equalation of input hashed with salt to hash etalon
        /// </summary>
        /// <param name="input">input to compare</param>
        /// <param name="salt">salt</param>
        /// <param name="hash">etalon</param>
        /// <returns></returns>
        public static bool ValidateHash(byte[] input, byte[] salt, byte[] hash)
        {
            var hashToCompare = GenerateSaltedHash(input, salt);

            if (hash.Length != hashToCompare.Length)
            {
                return false;
            }

            for (int i = 0; i < hash.Length; i++)
            {
                if (hash[i] != hashToCompare[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Generates random token for oAuth authorization
        /// </summary>
        /// <returns></returns>
        public static string GenerateToken()
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[32];
                rng.GetBytes(tokenData);

                return Convert.ToBase64String(tokenData);
            }
        }
    }
}
