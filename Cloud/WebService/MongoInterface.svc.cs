﻿using Core;
using Core.ApiModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WebService
{
    public class MongoInterface : IMongoInterface
    {
        public IEnumerable<Device> GetActions()
        {
            return new MongoDbController().GetActions();
        }

        public IEnumerable<Device> GetDevices()
        {
            return new MongoDbController().GetDevices();
        }

        public IEnumerable<SensorData> GetActualData()
        {
            return new MongoDbController().GetActualData();
        }

        public Task<IEnumerable<SensorData>> GetDateIntervalData(int from, int to, int type)
        {
            return new MongoDbController().GetDateIntervalData(from, to, type);
        }

        public void UpdateDevice(Device device)
        {
            new MongoDbController().UpdateDevice(device);
        }

        public void PostData(IEnumerable<SensorData> data)
        {
            new MongoDbController().InsertSensorsData(data);
        }

        public Task<IEnumerable<Image>> GetLastImages()
        {
            return new MongoDbController().GetLastImages();
        }

        public void SaveImage(Image img)
        {
            new MongoDbController().SaveImage(img);
        }
        
    }    
}
