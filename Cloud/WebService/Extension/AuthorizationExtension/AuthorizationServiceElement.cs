﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace Services.Extension.AuthorizationExtension
{
    public class AuthorizationServiceElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get
            {
                return typeof(AuthorizationAttribute);
            }
        }

        protected override object CreateBehavior()
        {
            return new AuthorizationAttribute();
        }
    }
}