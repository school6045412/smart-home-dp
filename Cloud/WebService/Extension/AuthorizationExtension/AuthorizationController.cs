﻿using System.ServiceModel.Web;

namespace Services.Extension.AuthorizationExtension
{
    public static class AuthorizationController
    {
        public static string GetUserToken()
        {
            return WebOperationContext.Current.IncomingRequest.Headers["AuthorizationUser"];
        }

        public static string GetDeviceToken()
        {
            return WebOperationContext.Current.IncomingRequest.Headers["AuthorizationDevice"];
        }

        public static string GetToken()
        {
            if (!string.IsNullOrEmpty(GetUserToken()))
                return GetUserToken();
            return "";
        }
    }
}