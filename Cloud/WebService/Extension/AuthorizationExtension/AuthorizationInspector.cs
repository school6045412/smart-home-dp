﻿using Core;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

namespace Services.Extension.AuthorizationExtension
{
    public class AuthorizationInspector : IParameterInspector
    {
        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {

        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            var deviceToken = AuthorizationController.GetDeviceToken();

            if (!AuthenticationController.AuthorizeDeviceToken(deviceToken))
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);

            return null;
        }
    }
}